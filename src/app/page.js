import Image from 'next/image'
import styles from './page.module.css'
import Header from '@/components/Header'
import Main from '@/components/Main/Index'
import Main2 from '@/components/Main/main2'
export default function Home() {
  return (
    <main className={styles.main}>
    
    <Header/>
    <Main />
    <Main2/>
    </main>
  )
}
