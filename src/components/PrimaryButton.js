import React from 'react'
import './Button.css'

export default function PrimaryButton(props) {
  return (
    <button className='primary'>
        {props.name}
    </button>
  )
}
