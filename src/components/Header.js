import Image from 'next/image'
import React from 'react'
import './Header.css';
import Link from 'next/link';
import PrimaryButton from './PrimaryButton';
import SecondryButton from './SecondryButton';

export default function Header() {
    
  return (
    < >
    <div className='bg_001A2E main'>
    <div className='logo'>
    <img src={"/images/Text_Logo.png"} alt='logo'/>
    </div>
    <div>
    <nav>
      <ul>
        <li>
          <Link href="/">
            Home
          </Link>
        </li>
        <li>
          <Link href="/about">
            About Us
          </Link>
        </li>
        <li>
          <Link href="/contact">
            Contact
          </Link>
        </li>
        <li>
          <Link href="/contact">
            Contact
          </Link>
        </li>
      </ul>
    </nav>
    </div>
    <div className='button_div'>
        <Image src='/images/united-kingdom.png' width={20} height={10} />
        <SecondryButton name= "LOGIN"/>
        <PrimaryButton name="GET STARTED"/>
    </div>
    </div></>
  )
}
