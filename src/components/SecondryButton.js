import React from 'react'
import './Button.css'
export default function SecondryButton(props) {
  return (
    <button className='secondry'>
        {props.name}
    </button>
  )
}
