import React from 'react'
// import './index.css'
import Image from 'next/image'
export default function Main2() {
  return (
    <div className='home'>
        <div className='right'>
          <Image src='/images/Logo2.png'width={80} height={80}/>
          
            <p className='largetext'>
              Let's build skills <br/>
              with <span class="image-container">
  <img src="/images/Group33.svg" alt="Image" />
  <span class="image-text">IFA</span>
</span>& learn <br/>
              without limits...
            </p>
            <p className='smalltext'>
              Take your learning to the next level.
            </p>
        
        </div>
        <div className='left'><img src='/images/Group1267.png'/></div>
    </div>
  )
}
